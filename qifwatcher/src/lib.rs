use std::error::Error;
use std::path::Path;
use std::process::Command;
use std::sync::mpsc::channel;
use std::time::Duration;

use notify::{DebouncedEvent, RecursiveMode, Watcher, watcher};

pub fn watch_dir(qif_dir: &Path, sql_dir: &Path) -> Result<(), Box<dyn Error>> {
  let (tx, rx) = channel();

  let mut watcher = watcher(tx, Duration::from_secs(10))?;
  watcher.watch(qif_dir, RecursiveMode::NonRecursive)?;

  loop {
    let event = rx.recv()?;

    match &event {
      DebouncedEvent::Create(qif_file) => process_file(qif_file, sql_dir),
      _ => Ok(()),
    }?;
  }
}

fn filter_file(file: &Path) -> bool {
  file.extension()
      .filter(|&ext| ext == "qif")
      .is_some()
}

fn process_file(qif_file: &Path, sql_dir: &Path) -> Result<(), Box<dyn Error>> {
  if !filter_file(qif_file) {
    println!("Ignoring file: {:?}", qif_file);
    return Ok(());
  }

  let output_file = Path::new(sql_dir)
      .join(qif_file.file_name().unwrap())
      .with_extension("sql");

  println!("qifparser {:?} {:?}", qif_file, output_file);

  Command::new("./qifparser")
      .arg(qif_file)
      .arg(output_file)
      .spawn()?;

  Ok(())
}
