use std::process;

use clap::{App, Arg};
use std::path::Path;

fn main() {
  let matches = App::new("QIF Watcher")
      .version("0.1.0")
      .author("Daniel Albert <daniel.albert.art@gmail.com>")
      .about("Watch a directory for new QIF entries and run QIF Parser for each")
      .arg(Arg::with_name("qif_dir")
          .help("Path to QIF directory")
          .required(true)
          .index(1))
      .arg(Arg::with_name("sql_dir")
          .help("Path to SQL directory")
          .required(true)
          .index(2))
      .get_matches();

  let qif_dir = matches.value_of("qif_dir")
      .map(|f| Path::new(f))
      .unwrap();
  let sql_dir = matches.value_of("sql_dir")
      .map(|f| Path::new(f))
      .unwrap();

  if let Err(e) = qifwatcher::watch_dir(qif_dir, sql_dir) {
    println!("Application error: {}", e);
    process::exit(1);
  }
}
