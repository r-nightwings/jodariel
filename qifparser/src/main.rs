use std::process;

use clap::{App, Arg};
use std::path::Path;

fn main() {
  let matches = App::new("QIF Parser")
      .version("0.1.0")
      .author("Daniel Albert <daniel.albert.art@gmail.com>")
      .about("Parse a QIF file and convert to SQL statements")
      .arg(Arg::with_name("input")
          .help("Path to QIF input file")
          .required(true)
          .index(1))
      .arg(Arg::with_name("output")
          .help("Path to SQL output file, or stdout if not specified")
          .required(false)
          .index(2))
      .get_matches();

  let input = matches.value_of("input")
      .map(|f| Path::new(f))
      .unwrap();
  let output = matches.value_of("output")
      .map(|f| Path::new(f));

  if let Err(e) = qifparser::parse_qif(input, output) {
    println!("Application error: {}", e);
    process::exit(1);
  }
}
