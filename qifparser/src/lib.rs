use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, BufWriter, prelude::*, stdout};
use std::path::Path;

use crate::model::{Record, SqlRecord};

mod model;

pub fn parse_qif(input: &Path, output: Option<&Path>) -> Result<(), Box<dyn Error>> {
  let file = File::open(input)?;
  let mut reader = BufReader::new(file);
  let mut writer = BufWriter::new(match output {
    Some(x) => {
      let path = Path::new(x);
      Box::new(File::create(&path)?) as Box<dyn Write>
    }
    None => Box::new(stdout()) as Box<dyn Write>,
  });

  writeln!(writer, "TRUNCATE TABLE public.category CASCADE;")?;
  writeln!(writer, "TRUNCATE TABLE public.account CASCADE;")?;

  let mut last_header = String::new();
  let mut last_account: Option<Record> = None;
  let mut data = HashMap::new();

  loop {
    let mut line = String::new();
    let read = reader.read_line(&mut line)?;
    if read == 0 { break; };

    let line = line.trim();

    if line.starts_with('!') {
      last_header = line.to_string();

      continue;
    }

    if line.eq("^") {
      let record = Record::new(&last_header, &data, &last_account);
      writeln!(writer, "{}", record.to_sql())?;

      match record {
        Record::Account { .. } => last_account = Some(record.clone()),
        _ => ()
      };

      data.clear();
      continue;
    }

    let key = line[0..1].to_string();
    let val = line[1..].to_string();
    data.insert(key, val);
  }

  Ok(())
}
