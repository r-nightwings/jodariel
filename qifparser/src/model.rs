use std::collections::HashMap;
use std::str;

use lazy_static::lazy_static;
use regex::Regex;
use string_template::Template;
use uuid::Uuid;

lazy_static! {
  static ref TRANSFER_PATTERN: Regex = Regex::new(r"^\[.*\]$").unwrap();
}

#[derive(Clone)]
pub enum Record {
  Category {
    code: String,
    name: String,
    kind: String,
  },

  Account {
    name: String,
  },

  Transaction {
    id: Uuid,
    account: String,
    category: Option<String>,
    date: String,
    amount: String,
  },
}

pub trait SqlRecord {
  fn to_sql(&self) -> String;
}

impl Record {
  pub fn new(header: &str, data: &HashMap<String, String>, ref_record: &Option<Record>) -> Record {
    match header {
      "!Type:Cat" => {
        let code = data.get("N").unwrap().clone();
        let kind = if data.contains_key("E") { "Expense" } else { "Income" };

        let pos = code.rfind(':')
            .map(|i| i + 1)
            .unwrap_or(0);

        let name = String::from(&code[pos..]);

        Record::Category {
          code,
          name,
          kind: kind.to_owned(),
        }
      }

      "!Account" => {
        let name = data.get("N").unwrap().clone();

        Record::Account {
          name,
        }
      }

      _ => {
        let ref_account = ref_record.as_ref().unwrap();
        let account = match ref_account {
          Record::Account { name, .. } => name,
          _ => panic!(),
        };
        let account = account.clone();

        let category = data.get("L").cloned();

        let date = data.get("D").unwrap().clone();
        let amount = data.get("T").unwrap().clone();

        Record::Transaction {
          id: Uuid::new_v4(),
          account,
          category,
          date,
          amount,
        }
      }
    }
  }
}

impl SqlRecord for Record {
  fn to_sql(&self) -> String {
    match self {
      Record::Category { code, name, kind } => {
        let template = Template::new(
          "INSERT INTO \"category\" (code, name, type) VALUES ('{{code}}', '{{name}}', '{{type}}');"
        );

        let mut args: HashMap<&str, &str> = HashMap::new();

        args.insert("code", &code);
        args.insert("name", &name);
        args.insert("type", &kind);

        template.render(&args)
      }

      Record::Account { name } => {
        let template = Template::new(
          "INSERT INTO \"account\" (name) VALUES ('{{name}}');"
        );

        let mut args: HashMap<&str, &str> = HashMap::new();

        args.insert("name", &name);

        template.render(&args)
      }

      Record::Transaction { id, account, category, date, amount } => {
        let template = Template::new(
          "INSERT INTO \"transaction\" (id, account, category, date, amount) \
          VALUES ('{{id}}', '{{account}}', {{category}}, '{{date}}', {{amount}});"
        );

        let mut args: HashMap<&str, &str> = HashMap::new();
        let id = id.to_string();
        let mut category_str = String::new();
        match category {
          Some(cat) => {
            if TRANSFER_PATTERN.is_match(cat) {
              category_str.push_str("NULL");
            } else {
              category_str.push_str("'");
              category_str.push_str(cat);
              category_str.push_str("'");
            }
          }
          None => category_str.push_str("NULL"),
        };

        args.insert("id", &id);
        args.insert("account", &account);
        args.insert("category", &category_str);
        args.insert("date", &date);
        args.insert("amount", &amount);

        template.render(&args)
      }
    }
  }
}


