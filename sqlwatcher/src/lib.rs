use std::error::Error;
use std::fs::File;
use std::path::Path;
use std::process::{Command, Stdio};
use std::sync::mpsc::channel;
use std::time::Duration;

use notify::{DebouncedEvent, RecursiveMode, Watcher, watcher};

pub fn watch_dir(sql_dir: &Path, db_host: &str, db_user: &str, db_name: &str) -> Result<(), Box<dyn Error>> {
  let (tx, rx) = channel();

  let mut watcher = watcher(tx, Duration::from_secs(10))?;
  watcher.watch(sql_dir, RecursiveMode::NonRecursive)?;

  loop {
    let event = rx.recv()?;

    match &event {
      DebouncedEvent::Create(sql_file) => process_file(sql_file, db_host, db_user, db_name),
      _ => Ok(()),
    }?;
  }
}

fn filter_file(file: &Path) -> bool {
  file.extension()
      .filter(|&ext| ext == "sql")
      .is_some()
}

fn process_file(sql_file: &Path, db_host: &str, db_user: &str, db_name: &str) -> Result<(), Box<dyn Error>> {
  if !filter_file(&sql_file) {
    println!("Ignoring file: {:?}", sql_file);
    return Ok(());
  }

  println!("psql -h {} -U {} {} < {:?}", db_host, db_user, db_name, sql_file);

  let sql_file = File::open(sql_file)?;

  Command::new("psql")
      .arg("-h")
      .arg(&db_host)
      .arg("-U")
      .arg(&db_user)
      .arg(&db_name)
      .stdin(sql_file)
      .stdout(Stdio::null())
      .stderr(Stdio::null())
      .spawn()?;

  Ok(())
}
