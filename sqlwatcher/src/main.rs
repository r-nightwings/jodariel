use std::process;

use clap::{App, Arg};
use std::path::Path;

fn main() {
  let matches = App::new("SQL Watcher")
      .version("0.1.0")
      .author("Daniel Albert <daniel.albert.art@gmail.com>")
      .about("Watch a directory for new SQL entries and run psql for each")
      .arg(Arg::with_name("sql_dir")
          .help("Path to SQL directory")
          .required(true)
          .index(1))
      .arg(Arg::with_name("db_host")
          .short("-d")
          .long("--dbname")
          .help("PostgreSQL host (default: \"localhost\")")
          .takes_value(true))
      .arg(Arg::with_name("db_user")
          .short("-U")
          .long("--username")
          .help("PostgreSQL username (default: \"postgres\")")
          .takes_value(true))
      .arg(Arg::with_name("db_name")
          .help("PostgreSQL database name")
          .required(true)
          .index(2))
      .get_matches();

  let sql_dir = matches.value_of("sql_dir")
      .map(|f| Path::new(f))
      .unwrap();

  let db_host = matches.value_of("db_host").unwrap_or("localhost");
  let db_user = matches.value_of("db_user").unwrap_or("postgres");
  let db_name = matches.value_of("db_name").unwrap();

  if let Err(e) = sqlwatcher::watch_dir(sql_dir, db_host, db_user, db_name) {
    println!("Application error: {}", e);
    process::exit(1);
  }
}
